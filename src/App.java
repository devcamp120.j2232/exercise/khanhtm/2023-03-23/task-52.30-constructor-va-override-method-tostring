import java.util.ArrayList;
import com.devcamp.j03_javabasic.s10.Person;

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<Person> arrayList = new ArrayList<>();
        //khởi tạo person với các tham số khác nhau
        Person person0 = new Person();
        Person person1 = new Person("Devcamp");
        Person person2 = new Person("Viet",44, 76.8);
        Person person3 = new Person("Nam", 35, 58.8, 10000000, new String[] {"Little Camel"});
        //thêm object person vào danh sách
        arrayList.add(person0);
        arrayList.add(person1);
        arrayList.add(person2);
        arrayList.add(person3);
        //In ra màn hình.
        for (Person person : arrayList) {
            System.out.println(person);
        }
    }
}
